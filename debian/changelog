php-horde-kolab-storage (2.2.4-4) unstable; urgency=medium

  * Team upload.
  * d/patches: Use upstream patch to suppress warnings.
  * d/patches: fix serialization tests.

 -- Anton Gladky <gladk@debian.org>  Sun, 25 Dec 2022 11:37:38 +0100

php-horde-kolab-storage (2.2.4-3) unstable; urgency=medium

  * Team upload.

  [ Mike Gabriel ]
  * d/watch: Switch to format version 4.

  [ Anton Gladky ]
  * d/salsa-ci.yml: use aptly, simplify.
  * d/control: Bump Standards-Version: to 4.6.1. No changes needed.
  * d/patches: fix some warnings due to php8.1 changes.
  * d/tests/control: allow stderr.

 -- Anton Gladky <gladk@debian.org>  Sat, 24 Dec 2022 00:28:06 +0100

php-horde-kolab-storage (2.2.4-2) unstable; urgency=medium

  * d/patches: Add 1010_phpunit-8.x+9.x.patch. Fix tests with PHPUnit 8.x/9.x.
  * d/t/control: Require php-imap.
  * d/t/control: Require php-horde-test (>= 2.6.4+debian0-6~).
  * d/t/control: Require php-horde-history.
  * d/t/control: Require php-horde-log.
  * d/t/control: Require php-horde-imap-client.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 21 Oct 2020 09:24:03 +0000

php-horde-kolab-storage (2.2.4-1) unstable; urgency=medium

  [ Juri Grabowski ]
  * New upstream version 2.2.4
  * Update d/copyright from git show upstream-sid

  [ Mike Gabriel ]
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.
  * d/salsa-ci.yml: Add file with salsa-ci.yml and pipeline-jobs.yml calls.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 13:23:22 +0200

php-horde-kolab-storage (2.2.3-5) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959337).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/upstream/metadata: Add file. Comply with DEP-12.
  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/copyright: Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 21 May 2020 08:56:09 +0200

php-horde-kolab-storage (2.2.3-4) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 20:32:48 +0200

php-horde-kolab-storage (2.2.3-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 18:32:54 +0200

php-horde-kolab-storage (2.2.3-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Fri, 06 Apr 2018 14:18:16 +0200

php-horde-kolab-storage (2.2.3-1) unstable; urgency=medium

  * New upstream version 2.2.3

 -- Mathieu Parent <sathieu@debian.org>  Mon, 19 Dec 2016 09:14:53 +0100

php-horde-kolab-storage (2.2.2-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Wed, 08 Jun 2016 09:01:14 +0200

php-horde-kolab-storage (2.2.2-1) unstable; urgency=medium

  * New upstream version 2.2.2

 -- Mathieu Parent <sathieu@debian.org>  Wed, 06 Apr 2016 09:03:24 +0200

php-horde-kolab-storage (2.2.1-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Mar 2016 21:35:34 +0100

php-horde-kolab-storage (2.2.1-1) unstable; urgency=medium

  * New upstream version 2.2.1

 -- Mathieu Parent <sathieu@debian.org>  Thu, 04 Feb 2016 08:25:52 +0100

php-horde-kolab-storage (2.2.0-1) unstable; urgency=medium

  * New upstream version 2.2.0

 -- Mathieu Parent <sathieu@debian.org>  Wed, 06 Jan 2016 09:04:45 +0100

php-horde-kolab-storage (2.1.4-4) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 08:19:44 +0200

php-horde-kolab-storage (2.1.4-3) unstable; urgency=medium

  * Replace @version@ before running tests

 -- Mathieu Parent <sathieu@debian.org>  Tue, 13 Oct 2015 02:40:16 +0200

php-horde-kolab-storage (2.1.4-2) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Mon, 10 Aug 2015 01:58:06 +0200

php-horde-kolab-storage (2.1.4-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 2.1.4

 -- Mathieu Parent <sathieu@debian.org>  Mon, 04 May 2015 23:09:23 +0200

php-horde-kolab-storage (2.1.1-3) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 15:31:10 +0200

php-horde-kolab-storage (2.1.1-2) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 16:04:57 +0200

php-horde-kolab-storage (2.1.1-1) unstable; urgency=medium

  * New upstream version 2.1.1

 -- Mathieu Parent <sathieu@debian.org>  Wed, 03 Sep 2014 07:53:03 +0200

php-horde-kolab-storage (2.1.0-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 22:38:38 +0200

php-horde-kolab-storage (2.1.0-1) unstable; urgency=medium

  * New upstream version 2.1.0

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Apr 2014 09:43:56 +0200

php-horde-kolab-storage (2.0.5-1) unstable; urgency=low

  * New upstream version 2.0.5

 -- Mathieu Parent <sathieu@debian.org>  Thu, 29 Aug 2013 08:10:24 +0200

php-horde-kolab-storage (2.0.4-2) unstable; urgency=low

  * Use pristine-tar

 -- Mathieu Parent <sathieu@debian.org>  Thu, 06 Jun 2013 09:26:59 +0200

php-horde-kolab-storage (2.0.4-1) unstable; urgency=low

  * New upstream version 2.0.4

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Apr 2013 15:59:52 +0200

php-horde-kolab-storage (2.0.3-1) unstable; urgency=low

  * New upstream version 2.0.3

 -- Mathieu Parent <sathieu@debian.org>  Thu, 10 Jan 2013 20:12:21 +0100

php-horde-kolab-storage (2.0.2-3) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 20:33:20 +0100

php-horde-kolab-storage (2.0.2-2) unstable; urgency=low

  * Fix Vcs-Browser and Horde dir

 -- Mathieu Parent <sathieu@debian.org>  Sun, 09 Dec 2012 16:12:23 +0100

php-horde-kolab-storage (2.0.2-1) unstable; urgency=low

  * Horde_Kolab_Storage package
  * Initial packaging (Closes: #695395)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 07 Dec 2012 22:06:03 +0100
